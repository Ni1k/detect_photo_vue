import type { RouteRecordRaw } from 'vue-router';

import { getCurrentUser } from 'vuefire';

export const loginPage = {
  path: 'login',
  name: 'login',
  component: () => import('./LoginPage.vue'),
  beforeEnter: async () => {
    const user = await getCurrentUser();

    console.log(user);
    if (!user) {
      return true;
    }

    return { name: 'photo' };
  },
} as RouteRecordRaw;
