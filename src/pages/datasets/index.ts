import type { RouteRecordRaw } from 'vue-router';

import { getCurrentUser } from 'vuefire';

import { datasetsPage } from './ui';

import { datasetPage } from './dataset';

export const datasetsPages: RouteRecordRaw = {
  path: 'datasets',
  name: 'datasets',
  component: () => import('./DatasetsPages.vue'),
  beforeEnter: async () => {
    const user = await getCurrentUser();

    if (user) return true;

    return { name: 'login' };
  },

  children: [datasetsPage, datasetPage],
};
