export * from './item-card';

export * from './add-datasets';

export * from './card-datasets';

import type { RouteRecordRaw } from 'vue-router';

export const datasetsPage: RouteRecordRaw = {
  path: '',
  name: 'datasets',
  component: () => import('./DatasetsPage.vue'),
};
