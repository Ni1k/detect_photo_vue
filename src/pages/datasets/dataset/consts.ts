export const crumbs = (title: string | undefined) => [
  {
    href: '/',
    name: 'Главная',
  },
  {
    name: title,
  },
];
