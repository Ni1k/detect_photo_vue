import type { RouteRecordRaw } from 'vue-router';
import { getCurrentUser } from 'vuefire';
import { detectVideoPage } from './ui';

export const detectVideoPages = {
  path: '/video',
  name: 'video',
  component: () => import('./DetectVideoPages.vue'),
  beforeEnter: async () => {
    const user = await getCurrentUser();

    if (user) return true;

    return { name: 'login' };
  },
  children: [detectVideoPage],
} as RouteRecordRaw;
