import type { RouteRecordRaw } from 'vue-router';

import { homePage } from './home';
import { detectVideoPages } from './detect-video';
import { datasetsPages } from './datasets';
import { loginPage } from './login';
import { historyPage } from './history';
import { trainPage } from './train';
import { settingsPage } from './settings';
export const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    component: () => import('../layouts/MainLayout.vue'),
    children: [
      homePage,
      detectVideoPages,
      loginPage,
      datasetsPages,
      historyPage,
      trainPage,
      settingsPage,
    ],
  },

  {
    path: '/:catchAll(.*)*',
    name: 'error',
    component: () => import('./ErrorNotFound.vue'),
  },
];
