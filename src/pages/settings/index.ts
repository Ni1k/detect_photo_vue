import type { RouteRecordRaw } from 'vue-router';

export const settingsPage = {
  path: 'settings',
  name: 'settings',
  component: () => import('./SettingsPage.vue'),
} as RouteRecordRaw;
