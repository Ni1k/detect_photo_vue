import type { RouteRecordRaw } from 'vue-router';

export const historyPage = {
  path: 'history',
  name: 'history',
  component: () => import('./HistoryPage.vue'),
} as RouteRecordRaw;
