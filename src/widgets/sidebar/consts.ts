import { NIcon } from 'naive-ui';
import folder from '@/assets/svg/folder.vue';
import machine from '@/assets/svg/machine.vue';

function renderIcon(icon: any) {
  return () => h(NIcon, { size: 28 }, { default: () => h(icon) });
}

export const options = [
  {
    label: 'Датасет',
    key: 'datasets',
    icon: renderIcon(folder),
    routeName: '/datasets',
    accentColor: '#18a058',
  },

  {
    label: 'Распознавание фото',
    key: 'photo',
    icon: renderIcon(folder),
    routeName: '/photo',
    accentColor: '#18a058',
  },
  {
    label: 'Инструкция',
    key: 'settings',
    icon: renderIcon(folder),
    routeName: '/settings',
    accentColor: '#18a058',
  },
  {
    label: 'История',
    key: 'history',
    icon: renderIcon(folder),
    routeName: '/history',
    accentColor: '#18a058',
  },

  // {
  //   label: 'Машинное обучение',
  //   key: 'train',
  //   icon: renderIcon(folder),
  //   routeName: '/train',
  //   accentColor: '#18a058',
  // },
  {
    label: 'Распознавание видео',
    key: 'video',
    icon: renderIcon(folder),
    routeName: '/video',
    accentColor: '#18a058',
  },
];
