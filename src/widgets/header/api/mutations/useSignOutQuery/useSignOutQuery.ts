import { signOut, type Auth } from 'firebase/auth';

const useSignOutQuery = (options = {}) =>
  useMutation({
    mutationFn: (payload: Auth) => signOut(payload),
    ...options,
  });

export default useSignOutQuery;
