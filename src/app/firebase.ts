import { initializeApp } from 'firebase/app';

// ... other firebase imports

export const firebaseApp = initializeApp({
  apiKey: 'AIzaSyDP-tp_IgdA8CngdQM1F7HNV1xMeuqr_no',
  authDomain: 'diplom-e5ddd.firebaseapp.com',
  projectId: 'diplom-e5ddd',
  storageBucket: 'diplom-e5ddd.appspot.com',
  messagingSenderId: '624640860396',
  appId: '1:624640860396:web:6ea573dff52df8c955c5c9',
  measurementId: 'G-2RBWDF9SKR',
});
