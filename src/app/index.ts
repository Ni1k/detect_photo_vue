import '../assets/main.scss';

import { createApp } from 'vue';
import { createPinia } from 'pinia';
import { VueQueryPlugin } from '@tanstack/vue-query';
import { VueFire, VueFireAuth, VueFireFirestoreOptionsAPI } from 'vuefire';

import naive from 'naive-ui';

import VueKonva from 'vue-konva';

import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';

import App from './App.vue';
import { router } from './providers';
import { firebaseApp } from './firebase';

const pinia = createPinia();

export const app = createApp(App);

app
  .use(pinia)
  .use(router)
  .use(VueKonva)
  .use(ElementPlus)
  .use(VueQueryPlugin)

  .use(naive)
  .use(VueFire, {
    // imported above but could also just be created here
    firebaseApp,
    modules: [
      // we will see other modules later on
      VueFireAuth(),
      VueFireFirestoreOptionsAPI(),
    ],
  });
